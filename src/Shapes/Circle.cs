namespace Shapes;

public sealed class Circle : IShape
{
	public Circle(double radius)
	{
		Radius = radius;
	}

	public double Radius { get; set; }
	public double Area => CalculateArea();

	private double CalculateArea()
	{
		return Math.PI * Math.Pow(Radius, 2);
	}
}