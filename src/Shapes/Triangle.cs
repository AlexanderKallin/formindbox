namespace Shapes;

public class Triangle : IShape
{
	public Triangle(double a, double b, double c)
	{
		A = a;
		B = b;
		C = c;
	}

	public double A { get; set; }
	public double B { get; set; }
	public double C { get; set; }
	public bool IsRightAngled => CalculateIsRightAngled();

	public double Area => CalculateArea();

	private double CalculateArea()
	{
		var p = (A + B + C) / 2;
		return Math.Sqrt(p * (p - A) * (p - B) * (p - C));
	}

	private bool CalculateIsRightAngled()
	{
		var powA = Math.Pow(A, 2);
		var powB = Math.Pow(B, 2);
		var powC = Math.Pow(C, 2);

		return Math.Abs(powA + powB - powC) < 0.00001d || Math.Abs(powA + powC - powB) < 0.00001d ||
		       Math.Abs(powC + powB - powA) < 0.00001d;
	}
}