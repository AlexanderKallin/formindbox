namespace Shapes;

public interface IShape
{
	public double Area { get; }
}