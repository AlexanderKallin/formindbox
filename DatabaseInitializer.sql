CREATE DATABASE shop;
GO;

USE shop;
GO;

CREATE TABLE products
(
  id INT IDENTITY,
  name NVARCHAR(150) NOT NULL,
  CONSTRAINT PK_products PRIMARY KEY (id)
);

CREATE TABLE categories
(
    id INT IDENTITY,
    name NVARCHAR(150) NOT NULL,
    CONSTRAINT PK_categories PRIMARY KEY (id)
);

CREATE TABLE productsCategories
(
    product_id INT NOT NULL,
    category_id INT NOT NULL,
    CONSTRAINT PK_productsCategories PRIMARY KEY (product_id, category_id),
    CONSTRAINT FK_productsCategories_products FOREIGN KEY (product_id) REFERENCES products,
    CONSTRAINT FK_productsCategories_categories FOREIGN KEY (category_id) REFERENCES categories
);

INSERT INTO products VALUES ('Apple Iphone'), ('Samsung Galaxy'), ('Xiaomi Mi 10');
INSERT INTO categories VALUES ('Smartphones'), ('Apple smartphones'), ('Samsung smartphones'), ('Xiaomi smartphones');

INSERT INTO productsCategories
SELECT
    id as product_id,
    (SELECT id FROM categories WHERE name = 'Smartphones') as category_id
FROM products;

INSERT INTO productsCategories
SELECT
    (SELECT id FROM products WHERE name LIKE 'Apple%') as product_id,
    (SELECT id FROM categories WHERE name LIKE 'Apple%') as category_id;

INSERT INTO productsCategories
SELECT
    (SELECT id FROM products WHERE name LIKE 'Samsung%') as product_id,
    (SELECT id FROM categories WHERE name LIKE 'Samsung%') as category_id;

INSERT INTO productsCategories
SELECT
    (SELECT id FROM products WHERE name LIKE 'Xiaomi%') as product_id,
    (SELECT id FROM categories WHERE name LIKE 'Xiaomi%') as category_id;

INSERT INTO products VALUES ('Razer mouse');
