using FluentAssertions;

namespace Shapes.UnitTests;

public class CircleTests
{
	[TestCase(2, 12.56637d)]
	[TestCase(10, 314.15926d)]
	public void ShouldReturnValidArea(double radius, double expectedResult)
	{
		IShape shape = new Circle(radius);

		shape.Area.Should().BeApproximately(expectedResult, .00001d);
	}
	
	[TestCase(2, 10, 314.15926d)]
	public void ShouldReturnValidAreaEvenIfRadiusChanged(double radius, double newRadius, double expectedResult)
	{
		var circle = new Circle(radius)
		{
			Radius = newRadius
		};

		circle.Area.Should().BeApproximately(expectedResult, .00001d);
	}
}