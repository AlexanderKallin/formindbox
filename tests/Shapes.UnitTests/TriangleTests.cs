using FluentAssertions;

namespace Shapes.UnitTests;

public class TriangleTests
{
	[TestCase(3, 4, 5, 6d)]
	[TestCase(5, 10, 10, 24.20614d)]
	public void ShouldReturnValidArea(double a, double b, double c, double expectedResult)
	{
		IShape shape = new Triangle(a, b, c);

		shape.Area.Should().BeApproximately(expectedResult, .00001d);
	}

	[TestCase(3, 4, 5, 5, 10, 10, 24.20614d)]
	public void ShouldReturnValidAreaEvenIfRadiusChanged(double a, double b, double c,
		double newA, double newB, double newC, double expectedResult)
	{
		var triangle = new Triangle(a, b, c)
		{
			A = newA,
			B = newB,
			C = newC
		};

		triangle.Area.Should().BeApproximately(expectedResult, .00001d);
	}

	[TestCase(7, 24, 25, true)]
	[TestCase(5, 10, 10, false)]
	public void ShouldDetermineThatTriangleIsRightAngled(double a, double b, double c, bool expectedResult)
	{
		var triangle = new Triangle(a, b, c);

		triangle.IsRightAngled.Should().Be(expectedResult);
	}
}