SELECT
    p.name,
    c.name
FROM products as p
LEFT JOIN productsCategories as pc ON p.id = pc.product_id
LEFT JOIN categories c on pc.category_id = c.id;
